<?php

namespace App\Http\Controllers;

use App\Models\Category;

use App\Models\Announcement;
use Illuminate\Http\Request;
use App\Models\AnnouncementImage;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\AnnouncementRequest;
use App\Jobs\ResizeImage;

class AnnouncementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //! LISTA ANNUNCI
    public function index()
    {   
        $announcements= Announcement::where('is_accepted', true)
        ->orderBy('created_at', 'desc')
        -> get();
        return view('announcement.indexAnnouncement', compact('announcements'));;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    //! CREA ANNUNCI
    public function create(Request $request)
    {   
        $categories = Category::all();
        $uniqueSecret = $request->old(
            'uniqueSecret',base_convert(sha1(uniqid(mt_rand())), 16, 36)
        );
        return view("announcement.createAnnouncement" , compact('categories', 'uniqueSecret'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //! DATABASE 

    public function store(AnnouncementRequest $request)
    {
        
        $a= Auth::user()->announcements()->create([

            'title'=>$request->input('title'), 
            'description'=>$request->input('description'), 
            'pricing'=>$request->input('pricing'), 
            'user_id'=>Auth::user()->id,
            'category_id'=> $request->category
        ]);
        
        $uniqueSecret = $request->input('uniqueSecret');

        $images= session()->get("images.{$uniqueSecret}", []);

        $removedImages= session()->get("removedimages.{$uniqueSecret}", []);

        $images =array_diff($images, $removedImages);
         
        foreach($images as $image){
            $i= new AnnouncementImage();

            $fileName = basename($image);
            $newFileName= "public/announcements/{$a->id}/{$fileName}";
            Storage::move($image,  $newFileName);

            dispatch(new ResizeImage($newFileName, 300, 150));

            $i->file = $newFileName;
            $i->announcement_id = $a->id;
            $i->save();
        }

        File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}"));
        
        return redirect('/')-> with('announcement.created.success', 'ok');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    //! DETTAGLI ANNUNCI
    public function show($announcementID)
    {
        $announcements = Announcement::all();

        foreach($announcements as $announcement){
            if ($announcement['id'] == $announcementID){
                return view('announcement.showAnnouncement', compact('announcement'));
            }
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    //! MODIFICA ANNUNCI
    public function edit(Announcement $announcement)
    {
        return view('announcement.editAnnouncement', compact('announcement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    //! AGGIORNA ANNUNCI
    public function update(Request $request, Announcement $announcement)
    {
        $announcement-> title= $request-> title;
        $announcement-> description= $request-> description;
        $announcement-> pricing= $request-> pricing;
        $announcement-> save();


        return redirect(route('indexAnnouncement'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    //! CANCELLA ANNUNCI
    public function destroy(Announcement $announcement)
    {
        //
    }
    //! UPLOAD IMMAGINI

    public function uploadImage(Request $request){
        $uniqueSecret = $request->input('uniqueSecret');
        $fileName = $request->file('file')->store("public/temp/{$uniqueSecret}");

        dispatch(new ResizeImage(
            $fileName, 80, 80
        ));
        session()->push("images.{$uniqueSecret}", $fileName);
       
        return response()->json(
            [
                'id' =>  $fileName
            ]
            );

    }

    public function removeImage(Request $request){

        $uniqueSecret = $request->input('uniqueSecret');
        $fileName = $request->input('id');
        session()->push("removedimages.{$uniqueSecret}", $fileName);

        Storage::delete($fileName);
        return response()->json('ok');

    }


    public function getImages(Request $request){
        $uniqueSecret = $request->input('uniqueSecret');
        $images= session()->get("images.{$uniqueSecret}", []);

        $removedImages= session()->get("removedimages.{$uniqueSecret}", []);

        $images =array_diff($images, $removedImages);
        $data=[];
        foreach ($images as $image) {

            $data[] = [
                'id' => $image,
                'src' => AnnouncementImage::getUrlByFilePath($image, 80, 80)
            ];
        }

        return response()->json($data);
    }

}
