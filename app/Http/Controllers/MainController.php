<?php

namespace App\Http\Controllers;

use App\Mail\AdminMail;
use App\Models\Category;
use App\Mail\ContactMail;
use App\Models\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MainController extends Controller
{
    public function goHome(){
        $announcements= Announcement::where('is_accepted', true)
            ->orderBy('created_at', 'desc')
            -> take(3)
            -> get();
        return view('welcome', compact('announcements'));
    }

    public function goCategoryList($name, $category_id){
        // filtra le categorie
        $category= Category::find($category_id);
        // passa le categorie filtrate con gli annunci allegati ad esse, e le impagina per 5 elementi a vista
        $announcements= $category-> announcements()
            ->where('is_accepted', true)
            ->orderBy('created_at', 'desc')
            -> paginate(5);
        return view('announcement.categoryAnnouncement', compact('category', 'announcements'));
    }

    public function search(Request $request){

        $q = $request->input('q');
        $announcements = Announcement::search($q)->where('is_accepted', true)->query(function ($builder){
            $builder->with(['category']);
        })->get();

        return view('search_results', compact('q', 'announcements'));

    }

    public function contactUs(){
        
        return view('contactUs');
    }

    public function contactSubmit(Request $request){

        $email = $request->input("email");
        $name = $request->input("name");
        $message = $request->input("message");
        $contact = compact("name", "message");
        $admin = compact("name","email","message");
        Mail::to($email)->send(new ContactMail($contact));
        Mail::to("admin@calm.it")->send(new AdminMail($admin));

        return redirect(route("goHome"))->with("message", "Grazie per averci contattato");
    }

    public function locale($locale){
        session()->put('locale', $locale);
        return redirect()->back();
    }

    public function goUserProfile(){
        return view('userProfile.userProfile');
    }


}
