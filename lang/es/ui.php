<?php

return [
    'welcome' => 'Bienvenido a presto.it',
    'portal' => 'Portal Investigación Anuncios',
    'search' => 'Buscar',
    'placeholder' => 'Buscar anuncios',
    'title' => 'Tìtulo',
    'description' => 'Descripciòn',
    'category' => 'Categorìa',
    'price' => 'Precio',
    'date' => 'Fecha',
    'name' => 'Nombre',
    'btn-card' => 'Detalles Anuncio',
    'ourAd' => 'Nuestros Anuncios',
    'insertAd' => ' Insertar Anuncio',
    'contact' => 'Contactos',
    'register' => 'Registrate',
    'login' => 'Acceder',
    'ads' => 'Annuncios'

];