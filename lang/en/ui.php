<?php

return [
    'welcome' => 'Welcome to presto.it',
    'portal' => 'Ads Portal Research',
    'search' => 'Search',
    'placeholder' => 'Search Ads',
    'title' => 'Title',
    'description' => 'Description',
    'category' => 'Category',
    'price' => 'Price',
    'date' => 'Date',
    'name' => 'Name',
    'btn-card' => 'Details Ad',
    'ourAd' => 'Our Ads',
    'insertAd' => ' Insert Ads',
    'contact' => 'Contact Us',
    'register' => 'Sign In',
    'login' => 'Login',
    'ads' => 'Our Ads'


];