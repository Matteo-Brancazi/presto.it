<?php

return [
    'welcome' => 'Benvenuto in presto.it',
    'portal' => 'Portale ricerca annunci',
    'search' => 'Cerca',
    'placeholder' => 'Cerca Annunci',
    'title' => 'Titolo',
    'description' => 'Descrizione',
    'category' => 'Categoria',
    'price' => 'Prezzo',
    'date' => 'Data',
    'name' => 'Nome',
    'btn-card' => 'Dettaglio Annuncio',
    'ourAd' => 'I Nostri Annunci',
    'insertAd' => ' Inserisci Annuncio',
    'contact' => 'Contatti',
    'register' => 'Registrati',
    'login' => 'Accedi',
    'ads' => 'Annunci'
];