<x-layout>
  <div class="container-fluid contact_section">
      <div class="row h-100 justify-content-center ">
        <div class="contact_form-bg" >  
          <div class="col-12 col-md-6 d-flex align-items-center justify-content-center logo_wrapper">
            <img class="contact_logo" src="./img/logoIcon.png" height="130" alt="">
          </div>
          <h3 class="text-center fw-bold display-5 mt-5" style="padding: 30x 0px 0px 0px;">Contattaci</h3>
          <h6 class="text-center mt-1" style="padding: 30x 0px 0px 0px;">Per qualsiasi informazione</h6>
              <form method="POST" action="{{route('contactSubmit')}}">
                  @csrf
          
                  <div class="mb-3">
                    <label  class="form-label mt-3 mb-1">Email address </label>
                    <input type="email" class="form-control" name="email" placeholder="inserisci la tua email">
                  </div>
                  <div class="mb-3">
                    <label  class="form-label mt-3 mb-1">Username </label>
                    <input type="text" class="form-control" name="name" placeholder="inserisci la tuo Username">
                  </div>
                  <div class="mb-3">
                    <label  class="form-label mt-3 mb-1">Messaggio</label>
                    <textarea class="form-control" name="message" cols="40" rows="5" placeholder="inserisci il tuo messaggio"></textarea>
                  </div>
                  <button type="submit" class="btn btn-personal form-control mt-4 fw-bold">INVIA</button>
                </form>
          </div>
        </div>
      </div>
  </div>
 

</x-layout>