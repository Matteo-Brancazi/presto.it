<x-layout>
    <div class="container">
        <div class="row justify-content-center my-5">
            <div class="col-6 text-center">
               <img  src="img/user_icon.svg" height="200px" alt="">
               <h1 class="display-5 fw-bold my-2">{{Auth::user()->name}}</h1>
               <p>From Campobasso</p>
            </div>
        </div>   
    </div>

</x-layout>