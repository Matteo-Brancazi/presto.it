<nav class="navbar navbar-expand-lg my_navbar py-3 px-lg-5 px-md-4 px-sm-1" id="my_nav">
  <div class="container-fluid px-5">
    <a class="navbar-brand " href="{{route('goHome')}}"><img src="img/logo_nav.png" class="nav_logo img-fluid" height="40px" alt=""></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"><i class="fa-solid fa-bars bars_icon"></i></span>
    </button>
    <div class="collapse navbar-collapse justify-content-between" id="navbarSupportedContent">
      
      
      <ul class="navbar-nav mb-2 mb-lg-0 ">
      </ul>
      
      <ul class="navbar-nav mb-2 mb-lg-0 justify-content-center">
        <li class="nav-item">
          <a class="nav-link nav_a mx-3 active" aria-current="page" href="{{route('goHome')}}">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active nav-link nav_a mx-3" aria-current="page" href="{{route('indexAnnouncement')}}">{{ __('ui.ads') }}</a>
        </li>
        <li class="nav-item">
          <a class="nav-link nav_a mx-3 active" aria-current="page" href="{{route('contactUs')}}">{{ __('ui.contact') }}</a>
        </li>
        
        <li class="nav-item">
          <a class="nav-link nav_a mx-3 active" aria-current="page" href="{{route('createAnnouncement')}}">{{ __('ui.insertAd') }}</a>
        </li>
        
        <li class="nav-item dropdown">
          <a class="nav-link mx-3 active dropdown-toggle nav_category" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Languages
          </a>
          <ul class="dropdown-menu language_menu" aria-labelledby="navbarDropdown">
            <li class="nav-item">
              @include('locale.locale', ['lang'=> 'it', 'nation' => 'it'])
            </li>
            <li class="nav-item">
              @include('locale.locale', ['lang'=> 'en', 'nation' => 'gb'])
            </li>
            <li class="nav-item">
              @include('locale.locale', ['lang'=> 'es', 'nation' => 'es'])
            </li>
          </ul>
        </li>
      </ul>
      
      @guest
      
      <ul class="navbar-nav mb-2 mb-lg-0 align-items-center">
        <li class="nav-item">
          <a class="nav-link active mx-3 nav_a"  aria-current="page" href="{{route('register')}}">{{ __('ui.register') }}</a>
        </li>
        <li class="nav-item">
          <a class="btn btn-primary log_button text-white"  aria-current="page"  href="{{route('login')}}">{{ __('ui.login') }}</a>            
        </li>
      </ul>
      
      
      @else 
      @if (Auth::user()->is_revisor)
      <ul class="navbar-nav mb-2 mb-lg-0 align-items-center">
        <li class="nav-item">
          <a class="nav-link nav_a active" href="{{ route('revisor.home')}}">Revisor Home
            <span class="text-primary">{{\App\Models\Announcement::ToBeRevisionedCount()}}</span>
          </a>
        </li> 
        @endif
      </ul>
      
      
      <ul class="navbar-nav mb-2 mb-lg-0 align-items-center">
        <li class="nav-item">
          <a class="nav-link nav-link active mx-3 nav_a" href="{{ route('logout') }}"onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a> <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none"> @csrf </form>
        </li>
        <li class="nav-item">
          <a class="btn btn-primary log_button text-white" aria-current="page" href="{{route('goUserProfile')}}">{{Auth::user()->name}}</a>
        </li>
      </ul>
      
      
      
      @endguest
      
      
      
    </div>
  </div>
</nav>