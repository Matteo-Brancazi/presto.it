  
  <!-- FOOTER -->
  <!-- ===================================================================== -->
  <div class="footer-dark">
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-md-3 item">
            <h3>Developers</h3>
            <ul>
              <li><a href="#">Cosimo Caputi</a></li>
              <li><a href="#">Rusu Nicolae</a></li>
              <li><a href="#">Matteo Brancazi</a></li>
              <li><a href="#">Simona Verrelli</a></li>
              <li><a href="#">Riccardo Pupa</a></li>
            </ul>
          </div>
          <div class="col-sm-6 col-md-3 item">
            <h3>Link Legali</h3>
            <ul>
              <li><a href="#">Condizioni</a></li>
              <li><a href="#">Privacy</a></li>
              <li><a href="#">Gestione Cookies</a></li>
            </ul>
          </div>
          <div class="col-md-6 item text">
            <h3>Presto.it</h3>
            <p>Praesent sed lobortis mi. Suspendisse vel placerat ligula. Vivamus ac sem lacus. Ut vehicula rhoncus elementum. Etiam quis tristique lectus. Aliquam in arcu eget velit pulvinar dictum vel in justo.</p>
          </div>
          <p class="copyright">Presto.it © 2022</p>
        </div>
      </div>
    </footer>
  </div>