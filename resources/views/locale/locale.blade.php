<form method="POST" action="{{ route('locale', $lang) }}">
    @csrf
    <button type="submit" class="nav-link" style="background-color:transparent" ><span class="flag-icon flag-icon-{{ $nation }}"></span>
    </button>
</form>
