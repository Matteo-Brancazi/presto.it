<x-layout>
    @if ($announcement)
        
    <div class="container-fluid">
        <div class="row h-100 justify-content-center">
            <div class="px-5"> 
                <h1 class="text-center fw-bold display-5 mt-5">Hai <span class="text-white bg-primary my-3 px-2 fw-bold">{{\App\Models\Announcement::ToBeRevisionedCount()}}</span> annunci in attesa della tua conferma</h1> 
                <div class="col-12 mt-5">
                    <div class="card">
                        <div class="card-header fw-bold">Annuncio # {{ $announcement->id }}</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <h4 class="text-primary">Utente</h4>
                                </div>
                                <div class="col-md-8">
                                    <h6>{{ $announcement->user->name }}</h6>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-4">
                                    <h4 class="text-primary">Email</h4>
                                </div>
                                <div class="col-md-8">
                                    <h6>{{ $announcement->user->email }}</h6>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-4">
                                    <h4 class="text-primary">Titolo </h4>
                                </div>
                                <div class="col-md-8">
                                    <h6>{{ $announcement->title }}</h6>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-4">
                                    <h4 class="text-primary">Descrizione </h4>
                                </div>
                                <div class="col-md-8">
                                    <p>{{ $announcement->description }}</p>
                                </div>
                            </div>
                            <br>
                            <div class="row mb-2">
                                <div class="col-md-4">
                                    <h4 class="text-primary">Prezzo </h4>
                                </div>
                                <div class="col-md-8">
                                    <h6>{{ $announcement->pricing }}</h6>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-4">
                                    <h4 class="text-primary">Immagini </h4>
                                </div>
                                <div class="col-md-8">
                                    @foreach ($announcement->images as $image )
                                    <div class="row mb-2">
                                        <div class="col-12 col-md-4 mt-3">
                                            <img class="img-fluid" src="{{$image->getUrl(300, 150)}}" class="rounded" alt="">
                                        </div>
                                        <div class="col-md-8">
                                            {{-- {{ $image->id }}
                                            <br> --}}
                                            {{-- {{ $image->file }}
                                            <br> --}}
                                            {{-- {{ Storage::url($image->file) }} --}}
                                        </div>
                                    </div>
                                    {{-- <div class="row mb-2">
                                        <div class="col-md-4">
                                            <img src="" alt="">
                                        </div>
                                        <div class="col-md-8">
                                            ... ... ...
                                        </div>
                                    </div>                              --}}
                                    @endforeach
                                </div>
                            </div>
                            
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row justify-content-around mt-5">
            
            <div class="col-md-3 mb-5">
                <form method="POST" action="{{ route('revisor.accept', $announcement->id) }}">
                    @csrf
                    <button type="submit" class="btn btn-success form-control mt-4 fw-bold">Accetta</button>
                </form>
            </div>
            <div class="col-md-3 mb-5">
                <form method="POST" action="{{ route('revisor.reject', $announcement->id) }}">
                    @csrf
                    <button type="submit" class="btn btn-danger form-control mt-4 fw-bold">Rifiuta</button>
                </form>
            </div>
        </div>
    @else
    <div class="container mt-5">
        <div class="row h-100 justify-content-center ">
            <div class="col-12 col-md-6 d-flex align-items-center justify-content-center logo_wrapper">
                <h1 class="text-primary">Non Ci Sono Più Annunci Da Revisionare</h1>
            </div>
            <div class="col-12 col-md-6 d-flex align-items-center justify-content-center logo_wrapper">

                <img class="img-fluid text-center" src="/img/done.jpg" alt="Not found">
            </div>
        </div>
    </div>
    @endif
</x-layout>