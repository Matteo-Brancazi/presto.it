<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>E-mail Confirm</title>
</head>
<body>
    <div class="container bg-dark">
        <div class="row">
            <h1 class="text-center text-warning fw-bold text-uppercase">Arrivata nuova mail da utente{{ $admin["name"] }}</h1>
            <p class="text-center text-white fw-bold">Questo è il suo indirizzo e-mail:{{ $admin["email"] }}</p>
            <p class="text-center text-white fw-bold">Questo è il suo messaggio :</p>
            <p class="text-center text-white fw-bold">{{$admin["message"] }}</p>
        </div>
    </div>
</body>
</html>