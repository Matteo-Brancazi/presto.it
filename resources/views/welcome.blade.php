<x-layout>
    
    @if (session('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
    @endif
    @if (session('announcement.created.success'))
    <div class="alert alert-success">
        Attendi che il revisore accetti l'annuncio per favore
    </div>
    @endif
    @if (session('access.denied.revisor.only'))
    <div class="alert alert-danger">
        accesso non consentito. solo per revisori
    </div>
    @endif
    
    
    
    <!-- HEADER -->
    <!-- =========================================================================== -->
    <header class="container-fluid provacss">
        <div class="row justify-content-evenly header_background">
            <div class="col-md-6 col-lg-5 col-sm-10 mt-5 header-left">
                <h1 class="mx-4">Presto.it</h1>
                <h2 class="mx-4 mt-0">Lorem Ipsum Dolor Sir Amet</h2>
                <p class="mx-4 p_size">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laudantium ratione placeat vitae alias laboriosam repellendus harum.</p>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-12 mt-5 text-end p-0 m-0">
                <lottie-player src="/lottie/pcHeader.json" background="transparent"  speed="1" class="img-fluid hlotie text-end" style="width: 800px; height: 700px;" loop controls autoplay></lottie-player>
            </div>
        </div>
        <div class="row">
            <img class="img-fluid headerWave" src="/img/headerWave3.svg" alt="">
        </div>
    </header>
    
    
    
    {{-- /////FINE HEADER!!!!!!!!!!!!!!!!!!!!!!!!!!!!! --}}
    
    
    
    <div class="container sb_first_container">
        <div class="row justify-content-center">
            <form class="col-lg-10 col-md-12" method="GET" action="{{ route('search') }}">
                <div class="p-1 bg-light mb-4 search_container">
                    <div class="input-group align-items-center">
                        
                        <input type="text" name="q" style="width:500px;" placeholder="{{ __('ui.placeholder') }}">
                        <div class="input-group-append">
                            
                            <button id="button-addon1" class="btn btn-link" type="submit">{{ __('ui.search') }}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        
    </div>
    
    <!-- ================================================================================ -->
    <!-- SECTION 2 (spedisci e ricevi) -->
    <!-- ================================================================================ -->
    <div class="container section_2">
        <div class="row justify-content-center ">
            <h2 class="display-4 fw-bold col-12 text-center">Spedisci E Ricevi Con <br><span class="blue_1 display-2">Presto.it</span></h2>
        </div>
        <div class="row justify-content-center mx-1 p_size">
            <p class="col-xl-7 col-md-10 col-sm-12 text-center p_size">Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit mollitia sunt sed voluptas quaerat explicabo nihil aliquam nisi voluptatem consectetur, repellat velit, voluptas quaerat explicabo nihil aliquam nisi voluptatem consectetur, repellat velit.</p>
        </div>
        
        <div class="mt-5 d-flex justify-content-center">
            <lottie-player src="./lottie/section2.json" background="transparent"  speed="1" class="img-fluid lottie_plane" style="width: 600px; height: 500px;" loop controls autoplay></lottie-player>
        </div>
        
    </div>
    <img class="clouds_section2" src="./img/clouds_section.svg" alt="">    
    
    
    <!-- CAROSELLO ULTIMI ANNUNCI -->
    <!-- ===================================================================== -->
    
    <div class="container mt-5 swiper_title">
        <div class="row">
            <h2 class="display-4 fw-bold col-12 text-center my-5">I Nostri Ultimi Annunci</h2>
            <div class="row justify-content-evenly">
                
                @foreach ($announcements as $announcement)
                
                <div class="card v_card my-5" style="width: 22rem;">
                    <p>
                        @foreach ($announcement->images as $image)
                        
                        <img src="{{$image->getUrl(300, 150)}}" class=" rounded card-img-top img-fluid v_img" alt="">
                        
                        @endforeach
                        {{ $announcement->body }}
                    </p>
                    <div class="card-body">
                        <div class="d-flex justify-content-between v_card_title_price align-items-center">
                            <h5 class="card-title p-0 m-1"><span class="fw-bold"> </span>{{$announcement->title }}</h5>
                            <p class="p-0 m-1"><span class="fw-bold"></span>€ {{$announcement->pricing}} </p>
                        </div>
                        <a class="v_card_category" href="{{route('goCategoryList', [$announcement->category->name, $announcement->category->id])}}"><span class="fw-bold"></span>{{$announcement->category->name}}</a>
                        <p class="card-text v_card_description"><span class="fw-bold"></span>{{substr($announcement->description, 0, 50)}}</p>
                        <p class="card-text"><span class="fw-bold"></span>{{date_format($announcement->created_at, 'd/m/y')}}</p>
                        <div class="v_card_button">
                            <a href="{{route('showAnnouncement', $announcementID=$announcement['id'])}}" class="btn btn-primary fw-bold">{{ __('ui.btn-card')  }}</a>
                        </div>
                    </div>
                </div>  
                
                @endforeach
            </div>
        </div>     
    </div>
</div>
</div>


</x-layout>