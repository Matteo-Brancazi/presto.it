<x-layout>
  @if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  <!-- LOGIN VIEW WITH STYLE-->
  <section>
    <div class="container login_section">
      
      <div class="row h-100 justify-content-center">
        <div class="form-bg ">   
          <div class="col-12 col-md-6 d-flex align-items-center justify-content-center logo_wrapper">
            <img class="login_logo" src="./img/logoIcon.png" height="130" alt="">
          </div>
          <h3 class="my-title" style="padding: 30x 0px 0px 0px;">Accedi su <br><span class="blue_1 text-center display-4" style="line-height: 70px;">Presto.it</span></h3>
          <p class="my-slogan">Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
          <form id="stripe-login" method="POST" action="{{route('login')}}">
            
            @csrf
            
            <div class="mb-3">
              <label  class="form-label mt-3 mb-1">Email</label>
              <input type="email" class="form-control" name="email" placeholder="inserisci la tua email" >
            </div>
            <div class="mb-3">
              <label  class="form-label mt-3 mb-1">Password</label>
              <input type="password" class="form-control" name="password" placeholder="inserisci la tua password">
            </div>
            <div class="mb-3 ">
              <button type="submit" class="btn btn-personal form-control mt-4 fw-bold">ACCEDI</button>
            </div>
            <div class="footer-link mb-4">
              <p>Non hai un account?<a class="a_register" href="{{route('register')}}">Registrati</a></p>
            </div>
          </form>
        </div>
      </div>
      
    </div>
    
  </section>
  
  
  
  
  
  
</x-layout>