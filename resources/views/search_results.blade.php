<x-layout>
    <div class="container">
        <h1 class="text-center bg-warning my-5 fw-bold">Risultato ricerca : {{ $q }}</h1>
        <div class="row justify-content-evenly">
            @foreach ($announcements as $announcement)
              
              <div class="card col-3  my-5" style="width: 18rem;">
                <img src="https://picsum.photos/300/300" class="card-img-top" alt="not found">
                <div class="card-body">
                  <h5 class="card-title"><span class="fw-bold">Titolo: </span>{{$announcement->title }}</h5>
                  <p class="card-text"><span class="fw-bold">Descrizione: </span>{{ $announcement->description }}</p>
                  <p class="card-title"><span class="fw-bold">Prezzo: </span>{{$announcement->pricing}} €</p>
                  <p class="card-text"><span class="fw-bold">Categoria: {{$announcement->category->name}}</span></p>
                  <p class="card-text"><span class="fw-bold">Data: </span>{{date_format($announcement->created_at, 'd/m/y')}}</p>
                  <a href="{{route('showAnnouncement', $announcementID=$announcement['id'])}}" class="btn btn-primary fw-bold">Dettagli Annuncio</a>
                  </div>
                </div>  
            @endforeach
          </div>
    </div>
</x-layout>