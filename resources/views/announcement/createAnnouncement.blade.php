<x-layout>
  
  
  <div class="container-fluid mt-5">
    <div class="row h-100 justify-content-center ">
      <div class="contact_form-bg mt-5" >  
        <div class="col-12 col-md-6 d-flex align-items-center justify-content-center logo_wrapper">
          <img class="contact_logo" src="/img/logoIcon.png" height="130" alt="">
        </div>
        <h3 class="text-center fw-bold display-5 mt-5" style="padding: 30x 0px 0px 0px;">Inserisci il tuo annuncio</h3>         
        
        <form method="POST" action="{{route('storeAnnouncement')}}" enctype="multipart/form-data">
          @csrf
          <div class="card-body">
            <h3 class="d-none">DEBUG:: SECRET {{ $uniqueSecret }}</h3>
            <input type="hidden" name="uniqueSecret" value="{{ $uniqueSecret }}">
          </div>
          
          <div class="mb-3">
            <label class="form-label mt-3 mb-1">Titolo Annuncio :</label>
            <input type="text" class="form-control" name="title">
            
            {{-- MESSAGGIO DI ERRORE --}}    
            @error('title')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            
          </div>
          <div class="mb-3">
            <label class="form-label mt-3 mb-1">Descrizione :</label>
            <input type="text" class="form-control" name="description" >
            
            {{-- MESSAGGIO DI ERRORE --}}    
            @error('description')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            
          </div>
          <div class="mb-3">
            <label class="form-label mt-3 mb-1">Prezzo :</label>
            <input type="number" class="form-control" name="pricing" >
            
            {{-- MESSAGGIO DI ERRORE --}}    
            @error('pricing')
            <div class="alert alert-danger provaErrore">{{ $message }}</div>
            @enderror
          </div>
          
          
          <div class="form-group row">
            <label for="images" class="col-md-12-form-label text-md-right">Immagini</label>
            <div class="col-md-12">
              <div class="dropzone" id="drophere"></div>
              @error('images')
              <span class="invalid-feedback" role="alert">{{ $message }}</span>
              @enderror
            </div>
          </div>
          {{-- @dd(session("images.{$uniqueSecret}")) --}}
          
          <select name="category">
            @foreach($categories as $category)
            <option value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
          </select>
          <button type="submit" class="btn btn-primary m-5">Inserisci Annuncio</button>
        </form>
      </div>
    </div>
  </div>
</x-layout>