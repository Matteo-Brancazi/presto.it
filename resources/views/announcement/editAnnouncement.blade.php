<x-layout>

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>                        
          @endforeach
        </ul>
    </div>               
  @endif

  <div class="container">
      <div class="row">
          <div class="col-12 col-md-6">
              <h1 class="mt-5 display-3 text-center text-success fw-bold text-uppercase">Inserisci il tuo Annuncio</h1>
                <form method="POST" action="{{route('updateAnnouncement', compact('announcement'))}}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="mb-3">
                        <label class="form-label">Titolo Annuncio: </label>
                        <input type="text" class="form-control" name="title" value="{{$announcement['title']}}">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Descrizione: </label>
                        <input type="text" class="form-control" name="description" value="{{$announcement['description']}}">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Prezzo: </label>
                        <input type="number" class="form-control" name="pricing" value="{{$announcement['pricing']}}">
                    </div>
                    <button type="submit" class="btn btn-primary">Modifica</button>
                </form>

          </div>
      </div>
  </div>

</x-layout>