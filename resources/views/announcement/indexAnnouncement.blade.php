<x-layout>
  <div class="container">
    <div class="row">
      <img class="img-fluid" src="/img/bg-index-done.jpg" alt="">
    </div>
  </div>
  <div class="container">
    <h1 class="text-center my-5 text-uppercase display-3 text-primary">Annunci</h1>
    <div class="row justify-content-evenly">
      @foreach ($announcements as $announcement)
      
      <div class="card v_card my-5" style="width: 22rem;">
        <p>
          @foreach ($announcement->images as $image)

          <img src="{{$image->getUrl(300, 150)}}" class=" rounded card-img-top img-fluid v_img" alt="">
            
          @endforeach
          {{ $announcement->body }}
        </p>

        <div class="card-body">
          <div class="d-flex justify-content-between v_card_title_price align-items-center">
            <h5 class="card-title p-0 m-1">{{substr($announcement->title, 0, 13)}}</h5>
            <p class="p-0 m-1">€ {{$announcement->pricing}}</p>
          </div>
          {{-- <a class="v_card_category" href="{{route('goCategoryList', [$announcement->category->name, $announcement->category->id])}}">{{$announcement->category->name}}</a> --}}
          <p class="card-text v_card_description">{{substr($announcement->description, 0, 50)}}</p>
          <div class="v_card_button">
            <p class="card-text"><span class="fw-bold">Data: </span>{{date_format($announcement->created_at, 'd/m/y')}}</p>
            <a href="{{route('showAnnouncement', $announcementID=$announcement['id'])}}" class="btn btn-primary ">Dettagli</a>
          </div>
        </div>
      </div>
      
      @endforeach
      
      
    </div>
  </div>
</x-layout>

