<?php

use App\Models\Announcement;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\RevisorController;
use App\Http\Controllers\AnnouncementController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MainController::class, 'goHome'])-> name('goHome');

// !Rotte per gli annunci
Route::get('/announcement/indexAnnouncement', [AnnouncementController::class, 'index'])-> name('indexAnnouncement');
Route::get('/announcement/createAnnouncement', [AnnouncementController::class, 'create'])-> name('createAnnouncement');
Route::post('/announcement/storeAnnouncement', [AnnouncementController::class, 'store'])-> name('storeAnnouncement');
Route::get('/announcement/{announcementID}', [AnnouncementController::class, 'show'])-> name('showAnnouncement');
Route::get('/announcement/edit/{announcement}', [AnnouncementController::class, 'edit'])-> name('editAnnouncement');
Route::put('/announcement/update/{announcement}', [AnnouncementController::class, 'update'])-> name('updateAnnouncement');
Route::get('/category/{name}/{category_id}', [MainController::class, 'goCategoryList'])-> name('goCategoryList');
//!rotte per immagini
Route::post('/announcement/images/upload', [AnnouncementController::class, 'uploadImage'])->name('announcement.images.upload');
Route::delete('/announcement/images/remove', [AnnouncementController::class, 'removeImage'])->name('announcement.images.remove');
Route::get('/announcement/get/images', [AnnouncementController::class, 'getImages'])->name('announcement.images');


//!rotte per revisore
Route::get('/revisor/home' , [RevisorController::class, 'index'])->name('revisor.home');
Route::post('/revisor/announcement/{id}/accept', [RevisorController::class, 'accept'])->name('revisor.accept');
Route::post('/revisor/announcement/{id}/reject', [RevisorController::class, 'reject'])->name('revisor.reject');

//!rotte per contatti
Route::get('/contactUs', [MainController::class, 'contactUs'])->name('contactUs');
Route::post('/contactUs/submit', [MainController::class, 'contactSubmit'])->name('contactSubmit');

//!rotte per ricerca
Route::get('/search', [MainController::class, 'search'])->name('search');

//!rotte per linuaggi
Route::post('/locale/{locale}', [MainController::class, 'locale'])->name('locale');
//!rotte profilo utente
Route::get('/profile' , [MainController::class,'goUserProfile' ])->name('goUserProfile');