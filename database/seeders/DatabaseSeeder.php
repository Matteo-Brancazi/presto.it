<?php

namespace Database\Seeders;

use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
        
            ['name'=>'Abbigliamento'],
            ['name'=>'Arredamento'],
            ['name'=>'Cucina'],
            ['name'=> 'Elettronica'],
            ['name'=>'Immobili'],
            ['name'=>'Musica'],
            ['name'=>'Motori'],
            ['name'=>'Sport'],
        
        ];    
        foreach($categories as $category){
            DB::table('categories')->insert([
                'name'=> $category ['name'],
                //!carbon
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),
            ]);
        }
    }

}
